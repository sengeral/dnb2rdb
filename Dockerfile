FROM postgres:9.6

ENV POSTGRES_DB dnb2rdb
ENV POSTGRES_USER dnb2rdb
ENV POSTGRES_PASSWORD dnb2rdb

COPY dump.sql:/docker-entrypoint-initdb.d/init.sql

EXPOSE 5432