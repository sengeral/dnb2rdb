# DNB 2 RDB

Proof of concept. Dump contains first 200 000 of 16 393 102 Datasets. To run execute:

```
docker build -t dnb2rdb .
docker run -it --name dnb2rdb -p5432:5432 dnb2rdb
```

Now you can use any Postgres client to connect. For example:

```
psql -U dnb2rdb -h 0.0.0.0 -p 5432 dnb2rdb
```

# Credits

* https://github.com/michaelbrunnbauer/rdf2rdb
* https://github.com/lehkost/DNBTitel-Elasticsearch
